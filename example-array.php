<?php

    $br ="<br>";
    //Example of indexed array;
    $number=array(1,2,3,4,5,6,7,8,9,10);
    var_dump($number);
    echo $br;

    //Example of short array syntax
    $number1=[10,9,8,7,6,5,4,3,2,1];
	var_dump($number1);
    echo $br;

    //Example of Associative array
    $number3=array("sub1"=>52, "sub2"=>81, "sub3"=>37, "sub4"=>43);
    var_dump($number3);
    echo $br;

    //Example of Short Associative array Syntax
    $number4=["sub5"=>49, "sub6"=>54, "sub7"=>73, "sub8"=>34];
    var_dump($number4);
    echo $br; 

	//Example of short array syntax
    $number5=[4,6,8,10,12,14,16,18,20];
	var_dump($number5);
    echo $br;
    
    array_unshift($number5,2); //This function Add numerical 2 as fisrt element;
    var_dump($number5);
    echo $br;
    
    array_push($number5,22); //This function add 22 as last elements;    
    var_dump($number5);
    echo $br;

    $number=array(1,2,3,4,5,6,7,8,9,10);
    var_dump($number);
    echo $br;
    $new_Number=array_shift($number);// Removes 1st element of an array
    var_dump($number);
    echo $br;
    var_dump($new_Number);
    echo $br;

    $NumericalValue=[8,6,14,1,2.5,50,11,35,5,99,];
    var_dump($NumericalValue);
    echo $br."Example of sort()".$br;
    sort($NumericalValue); // Ascending Sorting
    var_dump($NumericalValue);
    echo $br."Example of sort()".$br;
    rsort($NumericalValue); // Descending Sorting 
    var_dump($NumericalValue);

    var_dump(is_array($NumericalValue));
?>


